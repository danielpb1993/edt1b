package com.example.edt1b;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

public class MainActivity extends AppCompatActivity {

    private TextView textResult;
    private TextInputEditText textOp1;
    private TextInputEditText textOp2;
    private Button buttonAdd;
    private Button buttonSubtract;
    private Button buttonMult;
    private Button buttonDiv;
    private Button buttonInfo;
    private Button buttonPow;
    private Button buttonRes;

    Calculator calc = new Calculator();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //hooks
        textResult = findViewById(R.id.textResult);
        textOp1 = findViewById(R.id.textOp1);
        textOp2 = findViewById(R.id.textOp2);
        buttonAdd = findViewById(R.id.buttonAdd);
        buttonSubtract = findViewById(R.id.buttonSubtract);
        buttonMult = findViewById(R.id.buttonMult);
        buttonDiv = findViewById(R.id.buttonDiv);
        buttonInfo = findViewById(R.id.buttonInfo);
        buttonPow = findViewById(R.id.buttonPow);
        buttonRes= findViewById(R.id.buttonRes);

        //method1
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textOp1.getText().toString().trim().isEmpty() ||
                textOp2.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    double op1D = Double.parseDouble(textOp1.getText().toString().trim());
                    double op2D = Double.parseDouble(textOp2.getText().toString().trim());
                    textResult.setText(calc.add(op1D, op2D)+"");
                }
                //Log.e(MainActivity.class.getSimpleName(), "Error DP");
            }
        });

        buttonInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                startActivity(intent);
            }
        });

        buttonSubtract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textOp1.getText().toString().trim().isEmpty() ||
                        textOp2.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    double op1D = Double.parseDouble(textOp1.getText().toString().trim());
                    double op2D = Double.parseDouble(textOp2.getText().toString().trim());
                    textResult.setText(calc.subtract(op1D, op2D)+"");
                }
            }
        });

        buttonMult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textOp1.getText().toString().trim().isEmpty() ||
                        textOp2.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    double op1D = Double.parseDouble(textOp1.getText().toString().trim());
                    double op2D = Double.parseDouble(textOp2.getText().toString().trim());
                    textResult.setText(calc.mult(op1D, op2D)+"");
                }
            }
        });

        buttonDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textOp1.getText().toString().trim().isEmpty() ||
                        textOp2.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else if (Double.parseDouble(textOp2.getText().toString().trim()) == 0) {
                    Toast.makeText(MainActivity.this, "Divide by zero impossible", Toast.LENGTH_SHORT).show();
                } else {
                    double op1D = Double.parseDouble(textOp1.getText().toString().trim());
                    double op2D = Double.parseDouble(textOp2.getText().toString().trim());
                    textResult.setText(calc.div(op1D, op2D)+"");
                }
            }
        });

        buttonPow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textOp1.getText().toString().trim().isEmpty() ||
                        textOp2.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    double op1D = Double.parseDouble(textOp1.getText().toString().trim());
                    double op2D = Double.parseDouble(textOp2.getText().toString().trim());
                    textResult.setText(calc.pow(op1D, op2D) + "");
                }
            }
        });

        buttonRes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textResult.setText("0.0");
            }
        });
    }

    //method2
    /*public void buttonAddClick(View view) {
        double op1D = Double.parseDouble(textOp1.getText().toString().trim());
        double op2D = Double.parseDouble(textOp2.getText().toString().trim());

        textResult.setText(calc.add(op1D, op2D) + "");
    }*/
}